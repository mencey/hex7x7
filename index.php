<?php 
	session_start();
	require_once ('functions.php');
?>

<html>
<head>
<title>Hex</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="estilo.css" type="text/css" />
</head>
<body>
<div id="centro">
<div id="cabecera"></div>
<div class="listado">
<p><a href="about.php">About this game</a></p>
<p><a href="index.php">Restart game</a></p>
</div>
<div id="cuadro">
<?php 
	if (isset($_POST['enviado'])) {
		$tablero = unserialize ($_POST['tablero-enviado']);
		actualiza (&$tablero, $_POST['marca'], 3);
		$posR = pos_real ($_POST['marca']);
		$pos = mueve ($tablero, $posR);
		actualiza (&$tablero, $pos, 2);
		to_matrix ($tablero, &$matrix);
	}
	else {
		$tablero = inicializa_tablero ();
		$matrix = inicializa_matrix ();
		$_SESSION['LocalGames'] = array ();
		inicializa_games ();
	}
	mostrar ($tablero, $matrix);
//	echo '<br><br><h1>';
//	print_r ($_SESSION['LocalGames']);
//	echo '</h1>';
	if (count ($_SESSION['LocalGames']) < 1) {
		echo '<div id="mensaje">YOU LOSE</div>';
	}
?>
</div>

</div>
<div class="pie">This game is based on <a href="hexpaper.pdf">this paper</a>. Programed by <a href="http://www.eduardonacimiento.com">Eduardo Nacimiento Garcia</a>
<br /> Licenced under <a href="http://www.gnu.org/licenses/agpl.html">AGPL</a>, you can download it <a href="http://gitorious.org/mencey/hex7x7">freely</a></span>
</body>
</html>
